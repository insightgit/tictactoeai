public class AdversarialSearch {
    // This class's code is
    // Based upon the Aritifical Intelligence: A modern approach's
    // psuedocode in section 5.3 on Alpha Beta pruning.


    private static final int DEPTH_LIMIT = 6;


    private static int terminalUtility(Board board,
                                       Board.Mark winner,
                                       boolean isXTurn, int depth) {
        if(winner.equals(Board.Mark.BLANK) && depth >= DEPTH_LIMIT) {
            int value = board.value(isXTurn, depth);
            return value;
        } else if(isXTurn) {
            if(winner.equals(Board.Mark.X)) {
                return Integer.MAX_VALUE;
            } else {
                return Integer.MIN_VALUE;
            }
        } else {
            if(winner.equals(Board.Mark.O)) {
                return Integer.MAX_VALUE;
            } else {
                return Integer.MIN_VALUE;
            }
        }
    }

    // Kept this miniMax code in case you want to run this program
    // with mini-max. If you do, you will also need to get rid of
    // the alpha and beta arguments and references to them in the min
    // and max value functions.
    /*public static Board miniMaxDecision(Board board, boolean isXTurn) {
        // find an element with the max value of the min
        Board returnValue = null;
        int max = Integer.MIN_VALUE;
        Board[] possibleMoves = board.possibleMoves(isXTurn);

        for(Board move : possibleMoves) {
            if(move == null) {
                break;
            }

            int workingValue = minValue(move, !isXTurn, 1);

            if(workingValue > max) {
                returnValue = move;
                max = workingValue;
            }
        }

        return returnValue;
    }*/

    public static Board alphaBetaDecision(Board board, boolean isXTurn) {
        int v = maxValue(board, isXTurn, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
        Board[] moves = board.possibleMoves(isXTurn);

        for(int i = 0; 64 > i; ++i) {
            Board move = moves[i];
            if(move == null) {
                break;
            }

            int moveValue = move.value(isXTurn, 1);

            if(moveValue == v) {
                for(int x = 0; 4 > x; ++x) {
                    for(int y = 0; 4 > y; ++y) {
                        for(int z = 0; 4 > z; ++z) {
                            if(board.get(x, y, z).equals(Board.Mark.BLANK) &&
                               !move.get(x, y, z).equals(Board.Mark.BLANK)) {
                                System.out.println("My move (" + x + "," + y +
                                                   "," + z + ")");
                            }
                        }
                    }
                }

                return move;
            }
        }

        return null;
    }

    private static int maxValue(Board board, boolean isXTurn, int alpha,
                                int beta, int depth) {
        Board.Mark winner = board.whoWon();

        if(winner != Board.Mark.BLANK || DEPTH_LIMIT <= depth) {
            return terminalUtility(board, winner, isXTurn, depth);
        }

        int minBoardStateValue = -1;
        Board[] moves = board.possibleMoves(isXTurn);
        int v = Integer.MIN_VALUE;

        for(int i = 0; 64 > i; ++i){
            Board boardState = moves[i];

            if(boardState == null) {
                break;
            }

            int minValue = minValue(boardState, !isXTurn, alpha, beta,
                             depth + 1);

            if(minValue > v) {
                v = minValue;

                if(depth == 1) {
                    minBoardStateValue = boardState.value(isXTurn, depth);
                }
            }

            if(v >= beta) {
                return v;
            }

            alpha = Math.max(alpha, v);
        }

        if(depth == 1) {
            // if the board depth is 1 we return
            // minBoardStateValue because we will need to re-find this
            // board in the alpha beta decision function so we can't
            // return a board value that isn't immediately possible
            // from the state that we are currently in. We return
            // Integer.MIN_VALUE if it is equal to -1 because then
            // our move doesn't matter because we are in a forced win
            // scenario.
            if(minBoardStateValue == -1){
                return Integer.MIN_VALUE;
            } else {
                return minBoardStateValue;
            }
        } else {
            return v;
        }
    }

    private static int minValue(Board board, boolean isXTurn, int alpha,
                                int beta, int depth) {
        Board.Mark winner = board.whoWon();

        if(winner != Board.Mark.BLANK || DEPTH_LIMIT <= depth) {
            return terminalUtility(board, winner, isXTurn, depth);
        }

        int v = Integer.MAX_VALUE;

        for(Board boardState : board.possibleMoves(isXTurn)){
            if(boardState == null) {
                break;
            }

            v = Math.min(v, maxValue(boardState, !isXTurn, alpha, beta,
                                     depth + 1));

            if(v <= alpha) {
                return v;
            }

            beta = Math.min(beta, v);
        }

        return v;
    }
}
