import java.util.Scanner;

public class Main {

    private static Board getUserMove(Board board, Board.Mark userMark) {
        Board returnValue = new Board(board);
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please enter your move(x,y,z):");

        int suggestedPosition = Position.position(scanner.nextLine());

        if(board.get(suggestedPosition) != Board.Mark.BLANK) {
            System.out.println("rEerEerEe Mark already occupied!");
            return getUserMove(board, userMark);
        }

        returnValue.set(suggestedPosition, userMark);

        return returnValue;
    }

    public static void main(String[] args) {
        boolean areWeX = true;
        boolean doWeGoFirst = true;
        Board.Mark userMark;

        for(String arg : args) {
            switch (arg.toLowerCase()){
                case "-first":
                    doWeGoFirst = true;
                    break;
                case "-second":
                    doWeGoFirst = false;
                    break;
                case "-x":
                    areWeX = true;
                    break;
                case "-o":
                    areWeX = false;
                    break;
            }
        }

        System.out.println("Are we playing first:" + doWeGoFirst);
        System.out.println("Are we X:" + areWeX);

        if(areWeX) {
            userMark = Board.Mark.X;
        } else {
            userMark = Board.Mark.O;
        }

        Board board = new Board();

        while (board.whoWon() == Board.Mark.BLANK) {
            if(doWeGoFirst) {
                board.print();
                board = getUserMove(board, userMark);

                if(board.whoWon() != Board.Mark.BLANK){
                    break;
                }

                // if you want to use minimax
                //board = AdversarialSearch.miniMaxDecision(board, !areWeX);
                board = AdversarialSearch.alphaBetaDecision(board, !areWeX);
            } else {
                board = AdversarialSearch.alphaBetaDecision(board, !areWeX);
                // if you want to use minimax
                //board = AdversarialSearch.miniMaxDecision(board, !areWeX);
                board.print();

                if(board.whoWon() != Board.Mark.BLANK){
                    break;
                }

                board = getUserMove(board, userMark);
            }
        }

        if((board.whoWon() == Board.Mark.X && areWeX) ||
           (board.whoWon() == Board.Mark.O && !areWeX)) {
            System.out.println("You won(yay?): ");
        } else if(board.whoWon() == null) {
            System.out.println("It's a tie!");
        } else {
            System.out.println("You lost(oh no): ");
        }

        System.out.println(board.getWinningLine());
    }
}
